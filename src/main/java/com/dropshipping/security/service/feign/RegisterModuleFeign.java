package com.dropshipping.security.service.feign;

import com.dropshipping.security.dto.ResponseDTO;
import com.dropshipping.security.service.feign.fallback.RegisterModuleFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "register-module", fallback = RegisterModuleFeignFallback.class)
public interface RegisterModuleFeign {

    @GetMapping("/public/users/{userName}")
    ResponseDTO findUserByName(@PathVariable("userName") String userName);
}
