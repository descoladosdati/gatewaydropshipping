package com.dropshipping.security.service.feign.fallback;

import com.dropshipping.security.dto.ResponseDTO;
import com.dropshipping.security.service.feign.RegisterModuleFeign;
import org.springframework.stereotype.Component;

@Component
public class RegisterModuleFeignFallback implements RegisterModuleFeign {

    @Override
    public ResponseDTO findUserByName(String userName) {
        return ResponseDTO.builder().message("Failed to authenticate token").build();
    }
}
