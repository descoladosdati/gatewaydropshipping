package com.dropshipping.security.service.feign;

import com.dropshipping.security.dto.ResponseDTO;
import com.dropshipping.security.service.feign.fallback.SecurityModuleFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "security-module", fallback = SecurityModuleFeignFallback.class)
public interface SecurityModuleFeign {

    @GetMapping("/users/verify/token/{token}")
    ResponseDTO verifyToken(@PathVariable("token") String token);
}
