package com.dropshipping.security.service.feign.fallback;

import com.dropshipping.security.dto.ResponseDTO;
import com.dropshipping.security.service.feign.SecurityModuleFeign;
import org.springframework.stereotype.Component;

@Component
public class SecurityModuleFeignFallback implements SecurityModuleFeign {

    @Override
    public ResponseDTO verifyToken(String token) {
        return ResponseDTO.builder().message("Failed to authenticate token").build();
    }
}
