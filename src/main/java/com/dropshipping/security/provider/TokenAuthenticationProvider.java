package com.dropshipping.security.provider;

import com.dropshipping.security.User;
import com.dropshipping.security.dto.ResponseDTO;
import com.dropshipping.security.provider.exception.TokenAuthenticationException;
import com.dropshipping.security.service.feign.RegisterModuleFeign;
import com.dropshipping.security.service.feign.SecurityModuleFeign;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

import static lombok.AccessLevel.PACKAGE;


@Component
@AllArgsConstructor(access = PACKAGE)
public class TokenAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private SecurityModuleFeign securityModuleFeign;

    @Autowired
    private RegisterModuleFeign registerModuleFeign;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        final Object token = usernamePasswordAuthenticationToken.getCredentials();
        Optional<ResponseDTO> response = Optional.ofNullable(securityModuleFeign.verifyToken((String) token));
        if (!response.isPresent()) {
            throw new TokenAuthenticationException("Cannot authentication token=" + token);
        }
        if (StringUtils.isEmpty(response.get().getUsername())) {
            throw new TokenAuthenticationException(response.get().getMessage());
        }
        response = Optional.ofNullable(registerModuleFeign.findUserByName(response.get().getUsername()));
        response.orElseThrow(() -> new UsernameNotFoundException("Cannot find user."));
        if(StringUtils.isEmpty(response.get().getId())) {
            throw new UsernameNotFoundException("Cannot find user.");
        }
        User user = User.builder()
                .id(response.get().getId())
                .password(response.get().getPassword())
                .username(response.get().getUsername())
                .build();
        return user;
    }
}
