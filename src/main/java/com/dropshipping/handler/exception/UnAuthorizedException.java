package com.dropshipping.handler.exception;

import javax.servlet.ServletException;

public class UnAuthorizedException extends ServletException {

    public UnAuthorizedException(String message) {
        super(message);
    }
}
