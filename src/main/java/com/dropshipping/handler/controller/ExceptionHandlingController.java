package com.dropshipping.handler.controller;

import com.dropshipping.handler.exception.UnAuthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ExceptionHandlingController {

    @ResponseStatus(value=HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnAuthorizedException.class)
    public String unAuthenticationToken(UnAuthorizedException e) {
        return e.getMessage();
    }
}
